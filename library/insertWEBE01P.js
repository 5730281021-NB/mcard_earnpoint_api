const config = require('./config');

module.exports.insertlog = (async function (dtf,res,req) {
    var MBBRH = req.body.POINTEARN_BRANCH;
    var MBDAT = dtf.substring(0,8);
    var MBHOR = dtf.substring(8,14);
    var MBCODE = req.body.MCARD_NUM;
    var MBDEP = "";
    if(typeof req.body.POINTEARN_DEPT != 'undefined'){
        MBDEP = req.body.POINTEARN_DEPT;
    }
    var MBAMT = req.body.POINTEARN_SALE_AMOUNT;
    var MBPOINT = req.body.POINTEARN_MPOINT_NORMAL;
    var MBPOINS = req.body.POINTEARN_MPOINT_SPECIAL;
    var MBFLG = "";
    if (typeof req.body.POINTEARN_FLAG != 'undefined') {
         MBFLG = req.body.POINTEARN_FLAG;
    }

    var insertquery = "insert INTO MBRFLIB/WEBE01P";
    insertquery += "MBBRH,MBDAT,MBHOR,MBCODE,MBDEP,MBAMT,MBPOINT,MBPOINS,MBFLG";
    insertquery += " value(?,?,?,?,?,?,?,?,?)"

    var insert_params = [MBBRH,MBDAT,MBHOR,MBCODE,MBDEP,MBAMT,MBPOINT,MBPOINS,MBFLG];
    try {
    var result = await pool.insertAndGetId(insertquery, insert_params);
    console.log(result.length);
    console.log(result);
    }
    catch (error) {
        console.log('error');
        console.log(error);
        res.status(200).send({
            "RESP_CDE": "304",
            "RESP_MSG": "Cannot Insert to Database"
        });
    }

    

})