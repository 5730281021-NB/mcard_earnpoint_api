const express = require('express')

const app = express();

const checkschema = require('./library/checkSchema');
const datetime = require('./library/datetime');
const lookupMcard = require('./library/checkMcard');
const verifypromo = require('./library/verifyPromotion');
const insertlog = require('./library/insertWEBE01P');

const bodyParser = require('body-parser');

app.listen(8127, function () {
	console.log('app listening on port 8127!');
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.post('/earnpoint', async function (req, res) {

    //check schema of inputs
    var dtf = await datetime.getdatetime();
    console.log(dtf);
    await checkschema.checkSchema(req,res,"earnpoint",dtf);

    var MCARD_NUM = req.body.MCARD_NUM;
    var POINTEARN_BRANCH = req.body.POINTEARN_BRANCH;
    var POINTEARN_DEPT = req.body.POINTEARN_DEPT;
    var POINTEARN_PROMOTION_NAME = req.body.POINTEARN_PROMOTION_NAME;
    var POINTEARN_PROMOTION_NUM = req.body.POINTEARN_PROMOTION_NUM;
    var POINTEARN_EDC_PROMO_NAME = req.body.POINTEARN_EDC_PROMO_NAME;
    var POINTEARN_EDC_SHOP_NAME = req.body.POINTEARN_EDC_SHOP_NAME;
    var POINTEARN_EDC_TERMINAL = req.body.POINTEARN_EDC_TERMINAL;
    var POINTEARN_MPOINT_NORMAL = req.body.POINTEARN_MPOINT_NORMAL;
    var POINTEARN_MPOINT_SPECIAL = req.body.POINTEARN_MPOINT_SPECIAL;
    var POINTEARN_SALE_AMOUNT = req.body.POINTEARN_SALE_AMOUNT;
    var POINTEARN_EDC_REF_NUM = req.body.POINTEARN_EDC_REF_NUM;
    var POINTEARN_APPV_NUM = req.body.POINTEARN_APPV_NUM;
    var POINTEARN_FLAG = req.body.POINTEARN_FLAG;

    //Check MBCODE in MVM01P
    var today = new Date();
	datexp = await (today.getFullYear().toString() + ((today.getMonth() + 1) < 10 ? '0' : '').toString() + (today.getMonth() + 1).toString());
    if(await lookupMcard.checkMcard(MCARD_NUM,datenow,res)){
        console.log("lookupMCard OK");
        //Check PROMOTION_NUM in Promotion master
        if(await verifypromo.verifyPromotion(POINTEARN_PROMOTION_NUM,dtf,res)){
            console.log("Promotion is Valid");
            //Check Promotion points is correct
            
            //INSERT LOG TABLE on WEBE01P
            await insertlog.insertlog(dtf,res,req);
            console.log("Insert OK");
            
        }
    }
})